import numpy as np
from .models import get_model
from .summary import Summary


def bb_compare(x1, x2, r_prior_alpha=0.5, r_prior_beta=0.5,
               nwarmup=20000, nsamples=20000):
    x1 = np.asarray(x1)
    x2 = np.asarray(x2)
    nsucc1 = (x1 == 1).sum()
    nfail1 = (x1 == 0).sum()
    nsucc2 = (x2 == 1).sum()
    nfail2 = (x2 == 0).sum()
    return bb_compare_counts(nsucc1=nsucc1,
                             nfail1=nfail1,
                             nsucc2=nsucc2,
                             nfail2=nfail2,
                             r_prior_alpha=r_prior_alpha,
                             r_prior_beta=r_prior_beta,
                             nwarmup=nwarmup,
                             nsamples=nsamples)


def bb_compare_counts(nsucc1, nfail1, nsucc2, nfail2,
                      nwarmup=20000,
                      nsamples=20000,
                      r_prior_alpha=0.5,
                      r_prior_beta=0.5):
    data = {
        'nsucc1': nsucc1,
        'nsucc2': nsucc2,
        'nfail1': nfail1,
        'nfail2': nfail2,
        'r_prior_alpha': r_prior_alpha,
        'r_prior_beta': r_prior_beta,
    }
    sm = get_model('bb_compare.stan')
    niter = nsamples + nwarmup
    fit = sm.sampling(data=data, iter=niter, warmup=nwarmup, chains=1)

    summary = Summary(fit, [('rate1', 'rate1'),
                            ('rate2', 'rate2'),
                            ('rate1-rate2', 'rate1_minus_rate2')])
    return summary


def test():
    x1 = [0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0]
    x2 = [1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, ]
    return bb_compare(x1, x2)
