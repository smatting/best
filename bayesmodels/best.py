import numpy as np
from .models import get_model
from .summary import Summary


def best_compare(x1, x2,
                 nsamples=20000,
                 nwarmup=20000,
                 mu_prior_mu=None,
                 mu_prior_sigma=None,
                 nu_minus_one_prior_lambda=1./29.,
                 sigma_prior_low=None,
                 sigma_prior_high=None):
    x1 = np.asarray(x1)
    x2 = np.asarray(x2)
    x = np.concatenate([x1, x2])

    if mu_prior_mu is None:
        mu_prior_mu = np.mean(x)

    if mu_prior_sigma is None:
        mu_prior_sigma = np.std(x) * 1000000

    if sigma_prior_low is None:
        sigma_prior_low = np.std(x) / 1000

    if sigma_prior_high is None:
        sigma_prior_high = np.std(x) * 1000

    data = {
        'x1': x1,
        'x2': x2,
        'n1': len(x1),
        'n2': len(x2),
        'mu_prior_mu': mu_prior_mu,
        'mu_prior_sigma': mu_prior_sigma,
        'sigma_prior_low': sigma_prior_low,
        'sigma_prior_high': sigma_prior_high,
        'nu_minus_one_prior_lambda': nu_minus_one_prior_lambda
    }
    sm = get_model('best.stan')
    niter = nsamples + nwarmup
    fit = sm.sampling(data=data, iter=niter, warmup=nwarmup, chains=1)

    summary = Summary(fit, [('log10(nu)', 'log10_nu'),
                            ('sigma1', 'sigma1'),
                            ('sigma2', 'sigma2'),
                            ('sigma1-sigma2', 'sigma1_minus_sigma2'),
                            ('mu1', 'mu1'),
                            ('mu2', 'mu2'),
                            ('mu1-mu2', 'mu1_minus_mu2')])

    return summary


def test():
    x1 = [1.96, 2.06, 2.03, 2.11, 1.88, 1.88, 2.08, 1.93, 2.03,
          2.03, 2.03, 2.08, 2.03, 2.11, 1.93]

    x2 = [1.83, 1.93, 1.88, 1.85, 1.85, 1.91, 1.91, 1.85, 1.78, 1.91, 1.93,
          1.80, 1.80, 1.85, 1.93, 1.85, 1.83, 1.85, 1.91, 1.85, 1.91, 1.85,
          1.80, 1.80, 1.85]

    return best_compare(x1, x2, nwarmup=20000, nsamples=20000)
