import pystan
import pickle
import os
import codecs
from hashlib import md5
from pkg_resources import resource_filename


def cachedir():
    return os.path.expanduser('~/.bayesmodels')


def ensure_cachedir():
    path = cachedir()
    if not os.path.exists(path):
        os.makedirs(path)


def cache_abspath(filename):
    p = cachedir()
    return os.path.abspath(os.path.join(p, filename))


def read_model_file(model_filename):
    fn = resource_filename(__name__,
                           os.path.join('resources', model_filename))
    with codecs.open(fn, 'r', 'utf8') as f:
        return f.read()


def compile_model_cached(model_code):
    """Use just as you would `stan`"""
    code_hash = md5(model_code.encode('ascii')).hexdigest()
    ensure_cachedir()
    cache_filename = '{}.pkl'.format(code_hash)
    cache_path = cache_abspath(cache_filename)
    try:
        sm = pickle.load(open(cache_path, 'rb'))
    except:
        sm = pystan.StanModel(model_code=model_code, verbose=False)
        with open(cache_path, 'wb') as f:
            pickle.dump(sm, f)
    return sm


def get_model(model_filename):
    model_code = read_model_file(model_filename)
    return compile_model_cached(model_code)
