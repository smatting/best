data {
    real r_prior_alpha;
    real r_prior_beta;
    int<lower=0> nsucc1;
    int<lower=0> nsucc2;
    int<lower=0> nfail1;
    int<lower=0> nfail2;
}
transformed data {
    real alpha1 = r_prior_alpha + nsucc1;
    real alpha2 = r_prior_alpha + nsucc2;
    real beta1 = r_prior_alpha + nfail1;
    real beta2 = r_prior_alpha + nfail2;
}
parameters {
    real<lower=0.0, upper=1.0> rate1;
    real<lower=0.0, upper=1.0> rate2;
}
model {
    rate1 ~ beta(alpha1, beta1);
    rate2 ~ beta(alpha2, beta2);
}
generated quantities {
    real rate1_minus_rate2 = rate1 - rate2;
}
