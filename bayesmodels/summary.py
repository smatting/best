import numpy as np
from collections import OrderedDict


def hdi(x, cred_mass):
    x = np.sort(x)
    n = len(x)

    interval_idx_inc = int(np.floor(cred_mass * n))
    n_intervals = n - interval_idx_inc
    interval_width = x[interval_idx_inc:] - x[:n_intervals]

    if len(interval_width) == 0:
        raise ValueError('Too few elements for interval calculation')

    min_idx = np.argmin(interval_width)
    hdi_min = x[min_idx]
    hdi_max = x[min_idx + interval_idx_inc]
    return hdi_min, hdi_max


def digits(x):
    x = np.abs(x)
    if x == 0:
        return 1
    else:
        return int(np.ceil(np.log10(x)))


def format_exponent(number, exponent, extra_digits=0):
    '''
    show all digits up to `exponent` + `extra_digits`
    '''
    digits = extra_digits + 1
    if exponent <= 0:
        precision = max(0, (-exponent) + digits - 1)
        fmt = '{{:.{}f}}'.format(precision)
    else:
        if extra_digits >= exponent:
            precision = extra_digits - exponent
            fmt = '{{:.{}f}}'.format(precision)
        else:
            if np.abs(number) < 1:
                fmt = '{:.0f}'
            else:
                ndigits = digits(np.floor(np.abs(number)))
                precision = max(0, ndigits - exponent - 1 + extra_digits)
                fmt = '{{:.{}e}}'.format(precision)
    return fmt.format(number)


def format_tuple(x, y, extra_digits=2):
    exponent = digits(np.abs(y-x))
    sx = format_exponent(x, exponent, extra_digits)
    sy = format_exponent(y, exponent, extra_digits)
    s = '({}, {})'.format(sx, sy)
    return s


class SamplesSummary:
    def __init__(self, name, samples, cred_mass):
        self.name = name
        self.hdi = hdi(samples, cred_mass)
        self.mean = np.mean(samples)
        self.samples = samples


class Summary:
    def __init__(self, model,
                 variables=[('mu1', 'mu1')]):
        self.model = model
        summaries = OrderedDict()
        chains = model.sim['samples'][0].chains
        nwarmup = model.sim['warmup']
        for var_label, var_name in variables:
            summary = SamplesSummary(var_label, chains[var_name][:nwarmup],
                                     cred_mass=0.95)
            summaries[var_name] = summary
            setattr(self, var_name, summary)
        self.summaries = summaries

    def __repr__(self):
        s = ''
        for var_label, summary in self.summaries.items():
            exponent = digits(np.abs(summary.hdi[1] - summary.hdi[0]))
            fmt = '{}\n  HDI {}\n  Mean {}\n'
            s += fmt.format(summary.name,
                            format_tuple(*summary.hdi),
                            format_exponent(summary.mean, exponent, 2))
        name = self.__class__.__name__
        return '{}\n{}'.format(name, s)
