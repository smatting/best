.. bayesmodels documentation master file, created by
   sphinx-quickstart on Wed Sep 27 00:37:04 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to bayesmodels's documentation!
=======================================

bayesmodels is a Python library that provides standard Bayesian statistical models.

It contains:

* ``best_compare(x1, x2)``

  Compare the means and standard deviations of two groups if your data is Student-t distributed.
  `"Bayesian estimation supersedes the t test.", Kruschke, John K. (2013) <http://www.indiana.edu/~kruschke/BEST/>`_

* ``bb_compare(x1, x2)``

  Compare the rates of two groups using a Beta-Bernoulli model.



Basic Usage
-----------



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
